#include <rk4.h>
#include <cmath>
#include <algorithm>

using namespace std;

RK4::RK4(const RHS_type& f_, double t0_, const Vec& y0_): VarStepMethod(f_, t0_, y0_, 4)
{

}

double RK4::a(int i, int j) const
{
    if(i > j)
        return a_v[(i-1)*i/2+j];
    else 
        return 0.0;
}

void RK4::try_step()
{
    vector< Vec > k(4);
    Vec ys(y0.size());
    //Один шаг
    for(int s = 0; s < 4; s++)
    {
        for(int i = 0; i < ys.size(); i++)
        {
            ys[i] = y0[i];
            for(int j = 0; j < s; j++)
                ys[i] += tau * a(s, j) * k[j][i];
        }   
        k[s] = f(t + c[s] * tau, ys);
    }
    y = y0;
    for(int s = 0; s < 4; s++)
        for(int i = 0; i < y.size(); i++)
            y[i] += tau * b[s] * k[s][i];
        
    //Второй шаг
    for(int s = 0; s < 4; s++)
    {
        for(int i = 0; i < ys.size(); i++)
        {
            ys[i] = y[i];
            for(int j = 0; j < s; j++)
                ys[i] += tau * a(s, j) * k[j][i];
        }   
        k[s] = f(t + tau + c[s] * tau, ys);
    }
    for(int s = 0; s < 4; s++)
        for(int i = 0; i < y.size(); i++)
            y[i] += tau * b[s] * k[s][i];  
        
    //Двойной шаг
    for(int s = 0; s < 4; s++)
    {
        for(int i = 0; i < ys.size(); i++)
        {
            ys[i] = y0[i];
            for(int j = 0; j < s; j++)
                ys[i] += 2.0 * tau * a(s, j) * k[j][i];
        }   
        k[s] = f(t + c[s] * 2.0 *tau, ys);
    }
    w = y0;
    for(int s = 0; s < 4; s++)
        for(int i = 0; i < y.size(); i++)
            w[i] += 2 * tau * b[s] * k[s][i];
}

void RK4::calc_err()
{
    Vec d(y0.size());
    for(int i = 0;i < d.size();i++)
        //d[i] = max(1.0, max(abs(y0[i]), abs(y[i]+(y[i]-w[i])/((1<<p) - 1))));
        d[i] = 1;
    err = abs(y[0]-w[0])/d[0];
    for(int i = 0;i< y.size(); i++)
        err = max(err, abs(y[i]-w[i])/d[i]);
    err /= (1<<p)-1;
}

void RK4::approve_step()
{
    t += 2*tau;
    //y0 = y;
    for(int i = 0;i< y.size(); i++)
        y0[i] = y[i] + (y[i]-w[i])/((1<<p) - 1);
}

void RK4::integrate(double tfin, const function< void(double, const Vec&,double)>& obs)
{
    while(t < tfin-1e-6)
    {
        if(t + 2*tau > tfin)
            tau = (tfin-t)/2.0;
        auto pp = step();
        obs(pp.first, pp.second,err);
    }
}
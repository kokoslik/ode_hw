#include <varstepmethod.h>
#include <iostream>
#include <algorithm>
#include <cmath>

using namespace std;

VarStepMethod::VarStepMethod(const RHS_type& f_, double t0_, const Vec& y0_, int p_):f(f_), t(t0_),y0(y0_),p(p_)
{

}

VarStepMethod::~VarStepMethod()
{

}

void VarStepMethod::setFactors(double facmin_, double fac_, double facmax_)
{
    facmin = facmin_;
    fac = fac_;
    facmax = facmax_;
}

void VarStepMethod::setInitStep(double tau0_)
{
    tau = tau0_;
}

void VarStepMethod::setTol(double tol_)
{
    tol = tol_;
}

pair< double, Vec > VarStepMethod::step()
{
    Ntotal++;
    try_step();
    calc_err();
    if(err <= tol)
    {
        approve_step();
        tau *= min(rejected?1.0:facmax, max(facmin, fac * pow(tol/err,1.0/(p+1))));
        rejected = false;
        return make_pair(t, y0);
    }
    else
    {
        rejected = true;
        Nreject++;
        tau *= min(1.0, max(facmin, fac * pow(tol/err,1.0/(p+1))));
        return step();
    }
}

void VarStepMethod::integrate(double tfin, const function< void(double, const Vec&,double)>& obs)
{
    while(t < tfin-1e-6)
    {
        if(t + tau > tfin)
            tau = (tfin-t);
        auto pp = step();
        obs(pp.first, pp.second,err);
    }
}

void VarStepMethod::printStats() const
{
    cout<<"Ntotal = "<<Ntotal<<endl;
    cout<<"Nreject = "<<Nreject<<endl;
}

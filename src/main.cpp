#include <iostream>
#include <cmath>
#include <algorithm>
#include <chrono>
#include <rk4.h>
#include <dopri5.h>
using namespace std;


int main()
{
    auto start = std::chrono::system_clock::now();
    double t0 = 1.0 + 7 * 0.1,
           tfin = t0 + 4.0;
    auto f = [](double t, const Vec& y)
    {
        Vec res(2);
        res[0] = 2*t*y[0]* log(max(y[1], 0.001));
        res[1] = -2*t*y[1]* log(max(y[0],0.001));
        return res;
    };
    auto sol = [](double t)
    {
        Vec res(2);
        res[0] = exp(sin(t*t));
        res[1] = exp(cos(t*t));
        return res;
    };
    Vec y0 = sol(t0);
    //Задание 1 и половинка 2
    auto obs = [&](double t, const Vec& y, double err)
    {
        //cout<< t<<' ';
        //for(int i = 0;i < y.size(); i++)
            //cout<<y[i]<<' ';
        //cout<<endl;
        if(t>=tfin-1e-6)
        {
            cout<<"Final Time = "<<t<<endl;
            Vec v = sol(t);
            cout<< "Error = "<< max(abs(v[0]-y[0]),abs(v[1]-y[1]))<<endl;
        }
    };
    
    auto obs2 = [&](double t, const Vec& y, double err)
    {
        cout<< t<<' ';
        for(int i = 0;i < y.size(); i++)
            cout<<y[i]<<' ';
        cout<<err<<' ';
        Vec v = sol(t);
        cout<<max(abs(v[0]-y[0]),abs(v[1]-y[1]));
        cout<<endl;
    };
    cout.precision(16);
    RK4 solver(f, t0, y0);
    solver.setFactors(0.5,0.7,2.0);
    solver.setTol(1e-6);
    solver.setInitStep(0.1);
    
    obs(t0,y0,0);
    solver.integrate(tfin, obs);
    solver.printStats();
    auto end = std::chrono::system_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
    //std::cout <<"Time : "<< elapsed.count() << endl;
    return 0;
}
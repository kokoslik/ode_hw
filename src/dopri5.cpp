#include <dopri5.h>
#include <cmath>
#include <algorithm>

using namespace std;

DoPri5::DoPri5(const RHS_type& f_, double t0_, const Vec& y0_): VarStepMethod(f_, t0_, y0_, 4)
{

}

double DoPri5::a(int i, int j) const
{
    if(i > j)
        return a_v[(i-1)*i/2+j];
    else 
        return 0.0;
}

void DoPri5::try_step()
{
    vector< Vec > k(7);
    Vec ys(y0.size());
    //Пятый порядок
    for(int s = 0; s < 7; s++)
    {
        for(int i = 0; i < ys.size(); i++)
        {
            ys[i] = y0[i];
            for(int j = 0; j < s; j++)
                ys[i] += tau * a(s, j) * k[j][i];
        }   
        k[s] = f(t + c[s] * tau, ys);
    }
    y = ys;
    //Четвертый порядок
    w = y0;
    for(int s = 0; s < 7; s++)
        for(int i = 0; i < y.size(); i++)
            w[i] += tau * b[s] * k[s][i];
}

void DoPri5::calc_err()
{
    Vec d(y0.size());
    for(int i = 0;i < d.size();i++)
        //d[i] = max(1.0, max(abs(y0[i]), abs(y[i])));
        d[i] = 1;
    err = abs(y[0]-w[0])/d[0];
    for(int i = 0;i< y.size(); i++)
        err = max(err, abs(y[i]-w[i])/d[i]);
}

void DoPri5::approve_step()
{
    t += tau;
    y0 = y;
}
#ifndef RK4_H
#define RK4_H
#include <varstepmethod.h>

class RK4: public VarStepMethod
{
private:
    const Vec a_v = {0.5, 
                     0.0, 0.5,
                     0.0, 0.0, 1.0},
                b = {1.0/6.0, 2.0/6.0, 2.0/6.0, 1.0/6.0},
                c = {0.0, 0.5, 0.5, 1.0};
    double a(int i, int j) const;
protected:
    virtual void try_step() override;
    virtual void approve_step() override;
    virtual void calc_err() override;
public:
    explicit RK4(const RHS_type& f_, double t0_, const Vec& y0_);
    virtual void integrate(double tfin, const std::function<void(double, const Vec&,double)> &obs) override;
};


#endif
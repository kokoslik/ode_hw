#ifndef VARSTEPMETHOD_H
#define VARSTEPMETHOD_H
#include <vector>
#include <functional>
#include <utility>

typedef std::vector< double > Vec;
typedef std::function< Vec(double, const Vec&) > RHS_type;

class VarStepMethod
{
private:
    double fac, facmin, facmax;
    double tol;
    int Ntotal = 0, Nreject = 0;
    bool rejected = false;
protected:
    RHS_type f;
    double tau;
    double t;
    double err;
    Vec y0, y, w;
    const int p;
    virtual void try_step() = 0;
    virtual void calc_err() = 0;
    virtual void approve_step() = 0;
public:
    explicit VarStepMethod(const RHS_type& f_, double t0_, const Vec& y0, int p_);
    virtual ~VarStepMethod();
    void setFactors(double facmin_, double fac_, double facmax_);
    void setTol(double tol_);
    void setInitStep(double tau0_);
    std::pair< double, Vec > step();
    virtual void integrate(double tfin, const std::function<void(double, const Vec&, double)> &obs);
    void printStats() const;
};

#endif